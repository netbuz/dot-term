"custom colorscheme
colorscheme etc8

"relative line numbering
set nu rnu

"search down into subfolders
set path+=**
"display all matching files
set wildmenu

"tab settings
set expandtab
set tabstop=8
set softtabstop=0
set shiftwidth=4
set smarttab

"syntax enanled
syntax enable
filetype plugin on

set backupdir=~/.config/nvim/tmp/backup
