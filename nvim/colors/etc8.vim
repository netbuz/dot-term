
" --------------------------------
set background=dark
" --------------------------------

highlight clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="etc8"

set t_Co=8

"----------------------------------------------------------------
" General settings                                              |
"----------------------------------------------------------------
"----------------------------------------------------------------
" Syntax group   | Foreground    | Background    | Style        |
"----------------------------------------------------------------

" --------------------------------
" Editor settings
" --------------------------------
hi Normal          ctermfg=none    ctermbg=none    cterm=none
hi Cursor          ctermfg=none    ctermbg=none    cterm=none
hi CursorLine      ctermfg=none    ctermbg=none    cterm=none
hi LineNr          ctermfg=0       ctermbg=none    cterm=bold
hi CursorLineNR    ctermfg=4       ctermbg=none    cterm=bold

" -----------------
" - Number column -
" -----------------
hi CursorColumn    ctermfg=none    ctermbg=none    cterm=none
hi FoldColumn      ctermfg=none    ctermbg=none    cterm=none
hi SignColumn      ctermfg=none    ctermbg=none    cterm=none
hi Folded          ctermfg=0    ctermbg=none    cterm=bold

" -------------------------
" - Window/Tab delimiters -
" -------------------------
hi VertSplit       ctermfg=none    ctermbg=none    cterm=none
hi ColorColumn     ctermfg=none    ctermbg=none    cterm=none
hi TabLine         ctermfg=none    ctermbg=none    cterm=none
hi TabLineFill     ctermfg=none    ctermbg=none    cterm=none
hi TabLineSel      ctermfg=none    ctermbg=none    cterm=none

" -------------------------------
" - File Navigation / Searching -
" -------------------------------
hi Directory       ctermfg=none    ctermbg=none    cterm=none
hi Search          ctermfg=0    ctermbg=3    cterm=none
hi IncSearch       ctermfg=0    ctermbg=3    cterm=none

" -----------------
" - Prompt/Status -
" -----------------
hi StatusLine      ctermfg=none    ctermbg=none    cterm=none
hi StatusLineNC    ctermfg=none    ctermbg=none    cterm=none
hi WildMenu        ctermfg=none    ctermbg=none    cterm=none
hi Question        ctermfg=none    ctermbg=none    cterm=none
hi Title           ctermfg=none    ctermbg=none    cterm=none
hi ModeMsg         ctermfg=none    ctermbg=none    cterm=none
hi MoreMsg         ctermfg=none    ctermbg=none    cterm=none

" --------------
" - Visual aid -
" --------------
hi MatchParen      ctermfg=none    ctermbg=none    cterm=none
hi Visual          ctermfg=none    ctermbg=none    cterm=none
hi VisualNOS       ctermfg=none    ctermbg=none    cterm=none
hi NonText         ctermfg=none    ctermbg=none    cterm=none

hi Todo            ctermfg=none    ctermbg=none    cterm=none
hi Underlined      ctermfg=none    ctermbg=none    cterm=none
hi Error           ctermfg=1    ctermbg=none    cterm=bold
hi ErrorMsg        ctermfg=1    ctermbg=none    cterm=bold
hi WarningMsg      ctermfg=3    ctermbg=none    cterm=bold
hi Ignore          ctermfg=none    ctermbg=none    cterm=none
hi SpecialKey      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Variable types
" --------------------------------
hi Constant        ctermfg=5    ctermbg=none    cterm=none
hi String          ctermfg=2    ctermbg=none    cterm=bold
hi StringDelimiter ctermfg=2    ctermbg=none    cterm=bold
hi Character       ctermfg=2    ctermbg=none    cterm=bold
hi Number          ctermfg=6    ctermbg=none    cterm=bold
hi Boolean         ctermfg=6    ctermbg=none    cterm=none
hi Float           ctermfg=6    ctermbg=none    cterm=bold

hi Identifier      ctermfg=4    ctermbg=none    cterm=bold
hi Function        ctermfg=4    ctermbg=none    cterm=bold

" --------------------------------
" Language constructs
" --------------------------------
hi Statement       ctermfg=5    ctermbg=none    cterm=none
hi Conditional     ctermfg=5    ctermbg=none    cterm=none
hi Repeat          ctermfg=3    ctermbg=none    cterm=none
hi Label           ctermfg=3    ctermbg=none    cterm=none
hi Operator        ctermfg=5    ctermbg=none    cterm=none
hi Keyword         ctermfg=4    ctermbg=none    cterm=bold
hi Exception       ctermfg=4    ctermbg=none    cterm=none
hi Comment         ctermfg=0    ctermbg=none    cterm=bold

hi Special         ctermfg=1    ctermbg=none    cterm=bold
hi SpecialChar     ctermfg=1    ctermbg=none    cterm=bold
hi Tag             ctermfg=3    ctermbg=none    cterm=bold
hi Delimiter       ctermfg=0    ctermbg=none    cterm=bold
hi SpecialComment  ctermfg=0    ctermbg=none    cterm=bold
hi Debug           ctermfg=4    ctermbg=none    cterm=none

" ----------
" - C like -
" ----------
hi PreProc         ctermfg=3    ctermbg=none    cterm=bold
hi Include         ctermfg=4    ctermbg=none    cterm=none
hi Define          ctermfg=5    ctermbg=none    cterm=none
hi Macro           ctermfg=5    ctermbg=none    cterm=bold
hi PreCondit       ctermfg=5    ctermbg=none    cterm=none

hi Type            ctermfg=2      ctermbg=none    cterm=none
hi StorageClass    ctermfg=3    ctermbg=none    cterm=none
hi Structure       ctermfg=5    ctermbg=none    cterm=none
hi Typedef         ctermfg=6    ctermbg=none    cterm=none

" --------------------------------
" Diff
" --------------------------------
hi DiffAdd         ctermfg=2    ctermbg=none    cterm=bold
hi DiffChange      ctermfg=3    ctermbg=none    cterm=none
hi DiffDelete      ctermfg=1    ctermbg=none    cterm=bold
hi DiffText        ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Completion menu
" --------------------------------
hi Pmenu           ctermfg=none    ctermbg=none    cterm=none
hi PmenuSel        ctermfg=none    ctermbg=2    cterm=bold
hi PmenuSbar       ctermfg=none    ctermbg=none    cterm=none
hi PmenuThumb      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Spelling
" --------------------------------
hi SpellBad        ctermfg=none    ctermbg=none    cterm=none
hi SpellCap        ctermfg=none    ctermbg=none    cterm=none
hi SpellLocal      ctermfg=none    ctermbg=none    cterm=none
hi SpellRare       ctermfg=none    ctermbg=none    cterm=none

