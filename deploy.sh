#!/bin/sh

# deploy.sh [ver 1.1.0] by netbu

path="$(cd "$(dirname "$0")"; pwd)"

link() {
    if [ ! -d "$3" ]; then
        mkdir -p "$3" || { printf "[FL#$1]: Directory $3 failed to be created.\n" && return 1 ; }
        printf "[OK#$1]: Directory $3 created.\n"
    else
        printf "[FL#$1]: Directory $3 already exists.\n"
    fi
    if [ ! -e "$3/$4" ]; then
        ln -s "$path/$1/$2" "$3/$4" || { printf "[FL#$1]: File/Directory $3/$4 failed to be linked.\n" && return 1 ; }
        printf "[OK#$1]: Symbolic link created.\n"
    else
        printf "[FL#$1]: File/Directory $3/$4 already exists.\n"
    fi
}

unlink() {
    if [ -h "$2" ]; then
        rm "$2" || { printf "[FL#$1]: Symbolic Link $2 failed to be deleted.\n" && return 1 ; }
        printf "[OK#$1]: Symbolic Link $2 removed.\n"
    else
        printf "[FL#$1]: Symbolic Link $2 doesn't exist.\n"
    fi
}

lncycle() {
    while [ $# -gt 0 ]; do
        case "$1" in
            all)
                set -- "blank" "shell" "tmux" "nvim" "mpd" ;;
            shell)
                link "shell" "shellrc" "$HOME" ".bashrc"
                ;;
            tmux)
                link "tmux" "tmux.conf" "$HOME" ".tmux.conf"
                ;;
            nvim)
                link "nvim" "colors" "$HOME/.config/nvim" "colors"
                link "nvim" "init.vim" "$HOME/.config/nvim" "init.vim"
                mkdir -p "$HOME/.config/nvim/tmp/backup"
                ;;
            mpd)
                link "mpd" "mpd.conf" "$HOME/.config/mpd" "mpd.conf"
                mkdir -p "$HOME/.config/mpd/playlists"
                link "ncmpcpp" "config" "$HOME/.config/ncmpcpp" "config"
                ;;
        esac
        shift
    done
}

ulncycle() {
    while [ $# -gt 0 ]; do
        case "$1" in
            all)
                set -- "blank" "shell" "tmux" "nvim" "mpd" ;;
            shell)
                unlink "shell" "$HOME/.bashrc"
                ;;
            tmux)
                unlink "tmux" "$HOME/.tmux.conf"
                ;;
            nvim)
                unlink "nvim" "$HOME/.config/nvim/colors"
                unlink "nvim" "$HOME/.config/nvim/init.vim"
                ;;
            mpd)
                unlink "mpd" "$HOME/.config/mpd/mpd.conf"
                unlink "ncmpcpp" "$HOME/.config/ncmpcpp/config"
                ;;
        esac
        shift
    done
}

while [ $# -gt 0 ]; do
    case "$1" in
        link)
            [ $# -gt 1 ] || { printf "Error: No target specified.\n" && exit 1 ; }
            shift
            lncycle $@
            set -- *
            exit 0 ;;
        unlink)
            [ $# -gt 1 ] || { printf "Error: No target specified.\n" && exit 1 ; }
            shift
            ulncycle $@
            set -- *
            exit 0 ;;
        *)
            printf "No valid options given.\n"
            exit 1 ;;
    esac
    shift
done
